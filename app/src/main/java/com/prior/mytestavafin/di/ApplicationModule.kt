package com.prior.mytestavafin.di

import android.content.Context
import androidx.room.Room
import com.prior.mytestavafin.data.database.MyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logging
import kotlinx.serialization.json.Json
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {
    @Singleton
    @Provides
    fun providesHttpClient(): HttpClient {
        val client = HttpClient(Android){
            install(Logging){
                level = LogLevel.ALL
            }
            install(JsonFeature){
                val json = Json {
                    ignoreUnknownKeys = true
                    encodeDefaults = true
                }
                serializer = KotlinxSerializer(json)
            }
        }

        return client
    }

    @Provides
    @Singleton
    fun providesMyDatabase(@ApplicationContext context: Context): MyDatabase{
        return Room.databaseBuilder(context, MyDatabase::class.java, MyDatabase.DATABASE_NAME)
            .build()
    }
}