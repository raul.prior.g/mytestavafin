package com.prior.mytestavafin.di

import com.prior.mytestavafin.data.RegisterRepositoryImp
import com.prior.mytestavafin.domain.RegisterRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class RegisterModule {
    @Binds
    @ViewModelScoped
    abstract fun provideRegisterRepository(repository: RegisterRepositoryImp): RegisterRepository
}