package com.prior.mytestavafin.di

import com.prior.mytestavafin.data.database.MyDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object RegisterVMModule {
    @ViewModelScoped
    @Provides
    fun providesRegisterDao(db: MyDatabase) = db.registerDao
}