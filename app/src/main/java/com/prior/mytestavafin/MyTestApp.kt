package com.prior.mytestavafin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyTestApp: Application()