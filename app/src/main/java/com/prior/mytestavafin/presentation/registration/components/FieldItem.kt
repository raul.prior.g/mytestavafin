package com.prior.mytestavafin.presentation.registration.components

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.prior.mytestavafin.domain.model.RegisterFieldData

@Composable
fun FieldItem(
    modifier:
    Modifier = Modifier,
    registerField: RegisterFieldData,
    onValueChange: (String, Boolean) -> Unit
) {
    Column(
        modifier = modifier
    ) {
        when(registerField.type){
            "text" -> TextField(registerField, onValueChange)
            "checkbox" -> CheckBoxField(registerField, onValueChange)
            else -> TextField(registerField, onValueChange)
        }
    }
}