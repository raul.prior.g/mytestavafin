package com.prior.mytestavafin.presentation.registration

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prior.mytestavafin.R
import com.prior.mytestavafin.core.Resource
import com.prior.mytestavafin.core.UiMessages
import com.prior.mytestavafin.domain.model.RegisterFieldData
import com.prior.mytestavafin.domain.RegisterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegistrationViewModel @Inject constructor(
    private val repository: RegisterRepository
): ViewModel() {
    private val _message = MutableStateFlow<UiMessages?>(null)
    val message = _message.asStateFlow()

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    private val _registerFields = mutableStateListOf<RegisterFieldData>()
    val registerFields: List<RegisterFieldData> = _registerFields

    init {
         viewModelScope.launch {
             repository.getRegisterFields().collect{ resource ->
                 when(resource){
                     is Resource.Error -> _message.value = resource.message
                     is Resource.Loading -> _isLoading.value = resource.isLoading
                     is Resource.Success -> {
                         _registerFields.addAll(
                             resource.data
                                 ?.sortedBy { it.order } ?: emptyList<RegisterFieldData>()
                                 .filter { it.visible }
                         )

                         _registerFields.distinctBy { it.type }.forEach{
                             Log.i(TAG, it.type)
                         }
                     }
                 }
             }
         }
    }

    fun onValueChange(text: String, selected: Boolean, field: RegisterFieldData){
        val index = _registerFields.indexOf(field)

        if(index in 0..registerFields.size){
            //Si existe un valor en maxlength, solo permito que se ingresen la cantidad de caracteres permitidos
            _registerFields[index] = field.copy(
                selected = selected,
                value = field.maxLength?.let {
                    if(text.length > it)
                        field.value
                    else
                        text
                } ?: text
            )
        }
    }

    fun onDismiss(){
        _message.value = null
    }

    fun onRegisterClick(){
        registerFields.forEach { field ->
            if(field.regex.isNotBlank()){
                val pattern = Regex(field.regex)
                if(!pattern.matches(field.value)){
                    _message.value = UiMessages.StringResource(
                        stringId = R.string.error_in_regex_validation,
                        field.name
                    )
                    return
                }
            }
        }

        _message.value = UiMessages.StringResource(
            stringId = R.string.success_registration
        )
    }

    companion object{
        const val TAG = "RegistrationViewModel"
    }
}