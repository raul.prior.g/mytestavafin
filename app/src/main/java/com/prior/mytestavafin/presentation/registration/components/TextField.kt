package com.prior.mytestavafin.presentation.registration.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.prior.mytestavafin.domain.model.RegisterFieldData

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TextField(
    registerField: RegisterFieldData,
    onValueChange: (String, Boolean) -> Unit
) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Text(
            text = registerField.name,
            style = MaterialTheme.typography.labelLarge,
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = registerField.value,
            onValueChange = { onValueChange(it, false) }
        )
    }
}