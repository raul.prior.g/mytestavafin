package com.prior.mytestavafin.presentation.registration

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.prior.mytestavafin.R
import com.prior.mytestavafin.core.MyMessageDialog
import com.prior.mytestavafin.core.components.LoadingCircularComp
import com.prior.mytestavafin.presentation.registration.components.FieldItem

@Composable
fun RegisterScreen(

) {
    val viewModel: RegistrationViewModel = hiltViewModel()
    val isLoading = viewModel.isLoading.collectAsStateWithLifecycle()
    val message = viewModel.message.collectAsStateWithLifecycle()
    val registerFields = viewModel.registerFields

    if(isLoading.value){
        LoadingCircularComp()
        return
    }

    message.value?.let { msg ->
        MyMessageDialog(
            message = msg.asString(),
            onDismiss = viewModel::onDismiss
        )
    }

    LazyColumn{
        item{
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = stringResource(id = R.string.registration_form),
                    style = MaterialTheme.typography.headlineLarge
                )
            }
        }
        items(registerFields){ field ->
            FieldItem(
                modifier = Modifier.fillMaxWidth(),
                registerField = field,
                onValueChange = { text, selected ->
                    viewModel.onValueChange(text, selected, field)
                }
            )
        }
        item {
            if(registerFields.isNotEmpty()){
                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Button(onClick = viewModel::onRegisterClick) {
                        Text(text = stringResource(id = R.string.regiter))
                    }
                }
            }
        }
    }

}