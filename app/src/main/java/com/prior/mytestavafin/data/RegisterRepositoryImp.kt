package com.prior.mytestavafin.data

import com.prior.mytestavafin.R
import com.prior.mytestavafin.core.Resource
import com.prior.mytestavafin.core.UiMessages
import com.prior.mytestavafin.data.database.RegisterDao
import com.prior.mytestavafin.data.database.entities.toDB
import com.prior.mytestavafin.data.network.RegisterServiceImp
import com.prior.mytestavafin.data.network.responses.RegisterFieldsResponse
import com.prior.mytestavafin.domain.model.RegisterFieldData
import com.prior.mytestavafin.domain.RegisterRepository
import com.prior.mytestavafin.domain.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RegisterRepositoryImp @Inject constructor(
    private val service: RegisterServiceImp,
    private val dao: RegisterDao
): RegisterRepository {
    //I usually use a Resource Generic class for return information with states for my UI
    override suspend fun getRegisterFields(): Flow<Resource<List<RegisterFieldData>>>{
        return flow {
            emit(Resource.Loading())

            val fieldsEntity = dao.getAll()
            val response = service.getRegisterFields()

            when(response){
                is Resource.Error -> {
                    emit(Resource.Loading(false))
                    if(fieldsEntity.isEmpty()){
                        emit(Resource.Error(
                            UiMessages.StringResource(R.string.fail_to_get_fields_from_remote_source))
                        )
                    }else{
                        emit(Resource.Success(fieldsEntity.map { it.toDomain() }))
                    }
                }
                is Resource.Loading -> { }
                is Resource.Success -> {
                    val fields = response.data as RegisterFieldsResponse

                    if(fields.ok != 1){
                        if(fieldsEntity.isEmpty()){
                            emit(Resource.Error(
                                UiMessages.StringResource(R.string.fail_to_get_fields_from_remote_source)
                            ))
                        }else{
                            emit(Resource.Success(fieldsEntity.map { it.toDomain() }))
                        }
                        emit(Resource.Loading(false))
                        return@flow
                    }

                    val fieldData = fields.toDomain()

                    fieldData.forEach {
                        dao.insert(it.toDB())
                    }

                    emit(Resource.Success(fieldData))
                }
            }

            emit(Resource.Loading(false))
        }
    }
}