package com.prior.mytestavafin.data.network.responses

import kotlinx.serialization.Serializable

@Serializable
data class CustomerFieldResponse(
    val order: Int,
    val visible: Boolean,
    val regex: String = "",
    val maxlength: Int? = 0,
    val type: String
)

