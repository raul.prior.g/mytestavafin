package com.prior.mytestavafin.data.network.request

import kotlinx.serialization.Serializable

@Serializable
data class RegisterFieldsRequest(
    val login: String,
    val password: String,
    val data: RegistrationDataRequest
)
