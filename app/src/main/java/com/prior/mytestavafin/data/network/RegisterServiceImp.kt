package com.prior.mytestavafin.data.network

import android.util.Log
import com.prior.mytestavafin.core.Resource
import com.prior.mytestavafin.core.UiMessages
import com.prior.mytestavafin.data.network.request.RegisterFieldsRequest
import com.prior.mytestavafin.data.network.request.RegistrationDataRequest
import com.prior.mytestavafin.data.network.responses.RegisterFieldsResponse
import io.ktor.client.HttpClient
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import javax.inject.Inject

class RegisterServiceImp @Inject constructor(
    private val client: HttpClient
): RegisterService {
    override suspend fun getRegisterFields(): Resource<RegisterFieldsResponse?> {
        val request = RegisterFieldsRequest(
            login = "testaffiliateexternal",
            password = "testaffiliateexternal",
            data = RegistrationDataRequest(true)
        )

        return try {
            val result = client.post<RegisterFieldsResponse>{
                url(HttpRoutes.REGISTRATION)
                contentType(ContentType.Application.Json)
                body = request
            }

            Resource.Success(result)
        }catch (e: Exception){
            Log.e(TAG, "getRegisterFields: ${e.message} \n ${e.printStackTrace()}")
            Resource.Error(UiMessages.DynamicMessage(e.message ?: ""))
        }
    }

    companion object {
        const val TAG = "RegisterServiceImp"
    }
}