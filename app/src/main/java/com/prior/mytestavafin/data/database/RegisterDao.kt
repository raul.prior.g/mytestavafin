package com.prior.mytestavafin.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.prior.mytestavafin.data.database.entities.RegisterFieldEntity

@Dao
interface RegisterDao {
    @Query("Select * From cat_register_fields")
    suspend fun getAll(): List<RegisterFieldEntity>

    @Insert(onConflict = REPLACE)
    suspend fun insert(registerFieldEntity: RegisterFieldEntity)

    @Delete
    suspend fun delete(registerFieldEntity: RegisterFieldEntity)
}