package com.prior.mytestavafin.data.network

import com.prior.mytestavafin.core.Resource
import com.prior.mytestavafin.data.network.responses.RegisterFieldsResponse

interface RegisterService {
    suspend fun getRegisterFields(): Resource<RegisterFieldsResponse?>
}