package com.prior.mytestavafin.data.network

object HttpRoutes {
    private const val BASE_URL = "https://platon.cf-it.at/affiliate"

    const val REGISTRATION = "$BASE_URL/getRegistrationFields"
}