package com.prior.mytestavafin.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prior.mytestavafin.domain.model.RegisterFieldData

@Entity(tableName = "cat_register_fields")
data class RegisterFieldEntity (
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    val name: String,
    val order: Int,
    val visible: Boolean,
    val regex: String,
    val maxLength: Int?,
    val type: String,
    val value: String,
    val selected: Boolean,
)

fun RegisterFieldData.toDB() =
    RegisterFieldEntity(
        name = name,
        order = order,
        visible = visible,
        regex = regex,
        maxLength = maxLength,
        type = type,
        value = value,
        selected = selected
    )