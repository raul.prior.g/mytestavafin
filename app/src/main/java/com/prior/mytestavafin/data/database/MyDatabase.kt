package com.prior.mytestavafin.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.prior.mytestavafin.data.database.entities.RegisterFieldEntity

@Database(
    entities = [
        RegisterFieldEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class MyDatabase: RoomDatabase() {
    abstract val registerDao: RegisterDao

    companion object{
        const val DATABASE_NAME = "my database"
    }
}