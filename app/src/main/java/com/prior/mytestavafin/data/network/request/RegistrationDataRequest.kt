package com.prior.mytestavafin.data.network.request

import kotlinx.serialization.Serializable

@Serializable
data class RegistrationDataRequest(
    val `new-registration`: Boolean
)
