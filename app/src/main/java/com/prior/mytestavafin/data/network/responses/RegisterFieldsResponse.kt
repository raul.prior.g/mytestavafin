package com.prior.mytestavafin.data.network.responses

import kotlinx.serialization.Serializable

@Serializable
data class RegisterFieldsResponse(
    val ok: Int,
    val data: Map<String, CustomerFieldResponse>
)