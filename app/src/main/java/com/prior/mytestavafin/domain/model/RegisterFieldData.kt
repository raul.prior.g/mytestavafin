package com.prior.mytestavafin.domain.model

import com.prior.mytestavafin.data.database.entities.RegisterFieldEntity
import com.prior.mytestavafin.data.network.responses.RegisterFieldsResponse

data class RegisterFieldData(
    val name: String,
    val order: Int,
    val visible: Boolean,
    val regex: String,
    val maxLength: Int?,
    val type: String,
    val value: String = "",
    val selected: Boolean = false
)

fun RegisterFieldsResponse.toDomain(): List<RegisterFieldData>{
    val fields = this.data
    val registerFieldsData = mutableListOf<RegisterFieldData>()

    for ((key, value) in fields.entries) {
        registerFieldsData.add(
            RegisterFieldData(
                name = key.replace("-", " ").uppercase(),
                order = value.order,
                visible = value.visible,
                regex = value.regex,
                maxLength = value.maxlength,
                type = value.type
            )
        )
    }

    return registerFieldsData
}

fun RegisterFieldEntity.toDomain() =
    RegisterFieldData(
        name = name,
        order = order,
        visible = visible,
        regex = regex,
        maxLength = maxLength,
        type = type,
        value = value,
        selected = selected
    )