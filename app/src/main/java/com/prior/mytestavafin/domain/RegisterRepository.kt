package com.prior.mytestavafin.domain

import com.prior.mytestavafin.core.Resource
import com.prior.mytestavafin.domain.model.RegisterFieldData
import kotlinx.coroutines.flow.Flow

interface RegisterRepository {
    suspend fun getRegisterFields(): Flow<Resource<List<RegisterFieldData>>>
}